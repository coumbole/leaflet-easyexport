var domtoimage = require('dom-to-image-h4');
var fileSaver = require('file-saver');

const areBoundsValid = bounds => {
  try {
    return bounds.isValid();
  } catch (error) {
    return false;
  }
}

L.Control.EasyPrint = L.Control.extend({
  options: {
    bounds: [[],[]],
    dpi: 220,
    title: 'Print map',
    position: 'topleft',
    sizeModes: ['3x4', '4x3', '1x1', '16x9', '9x16'],
    filename: 'map',
    hidden: true,
    tileWait: 500,
    hideControlContainer: true
  },

  onAdd: function () { 
    document.body.appendChild(this._getImageContainer());
    this.mapContainer = this._map.getContainer();
    this.options.sizeModes = this.options.sizeModes.map(function (sizeMode) {
      if (sizeMode === '3x4') {
        return {
          height: this._3by4.width,
          width: this._3by4.height,
          className: '3x4'
        }
      }
      if (sizeMode === '4x3') {
        return {
          height: this._3by4.height,
          width: this._3by4.width,
          className: '4x3'
        }
      }
      if (sizeMode === '1x1') {
        return {
          height: this._1by1.height,
          width: this._1by1.width,
          className: '1x1'
        }
      }
      if (sizeMode === '16x9') {
        return {
          height: this._16by9.height,
          width: this._16by9.width,
          className: '16x9'
        }
      }
      if (sizeMode === '9x16') {
        return {
          height: this._16by9.width,
          width: this._16by9.height,
          className: '9x16'
        }
      }
      return sizeMode;
    }, this);
    
    var container = L.DomUtil.create('div', 'leaflet-control-easyPrint leaflet-bar leaflet-control');
    if (!this.options.hidden) {

      var btnClass = 'leaflet-control-easyPrint-button'

      this.link = L.DomUtil.create('a', btnClass, container);
      this.link.id = "leafletEasyPrint";
      this.link.title = this.options.title;
      this.holder = L.DomUtil.create('ul', 'easyPrintHolder', container);

      this.options.sizeModes.forEach(function (sizeMode) {
        var btn = L.DomUtil.create('li', 'easyPrintSizeMode', this.holder);
        btn.title = sizeMode.name;
        var link = L.DomUtil.create('a', sizeMode.className, btn);
        L.DomEvent.addListener(btn, 'click', this.printMap, this);
      }, this);

      L.DomEvent.disableClickPropagation(container);
    }
    return container;
  },

  printMap: function (event, filename) {
    if (filename) {
      this.options.filename = filename
    }
    this.originalState = {
      mapWidth: this.mapContainer.style.width,
      widthWasPercentage: false,
      mapHeight: this.mapContainer.style.height,
      zoom: this._map.getZoom(),
      center: this._map.getCenter()
    };
    if (this.originalState.mapWidth.includes('%')) {
      this.originalState.percentageWidth = this.originalState.mapWidth;
      this.originalState.widthWasPercentage = true;
      this.originalState.mapWidth = this._map.getSize().x + "px";
    }
    this._map.fire("easyPrint-start", { event: event });
    this._map.fire("easyPrint-status-update", { status: "Map generation started" });
    if (this.options.hideControlContainer) {
      this._toggleControls();
    }
    var sizeMode = typeof event !== 'string' ? event.target.className : event;
    this.outerContainer = this._createOuterContainer(this.mapContainer);
    this._createImagePlaceholder(sizeMode);
  },

  _createImagePlaceholder: function (sizeMode) {
    var plugin = this;
    domtoimage.toPng(this.mapContainer, {
        width: parseInt(this.originalState.mapWidth.replace('px')),
        height: parseInt(this.originalState.mapHeight.replace('px'))
      })
      .then(function (dataUrl) {
        plugin.blankDiv = document.createElement("div");
        var blankDiv = plugin.blankDiv;
        plugin.outerContainer.parentElement.insertBefore(blankDiv, plugin.outerContainer);
        blankDiv.className = 'epHolder';
        blankDiv.style.backgroundImage = 'url("' + dataUrl + '")';
        blankDiv.style.position = 'absolute';
        blankDiv.style.zIndex = 1011;
        blankDiv.style.display = 'initial';
        blankDiv.style.width = plugin.originalState.mapWidth;
        blankDiv.style.height = plugin.originalState.mapHeight;
        plugin._resizeAndPrintMap(sizeMode);
      })
      .catch(function (error) {
        plugin._map.fire("easyPrint-failed", { error: error });
      });
  },

  _resizeAndPrintMap: function (sizeMode) {
    this._map.fire("easyPrint-status-update", { status: "Scaling map up to sufficient resolution" });
    this.outerContainer.style.opacity = 0;
    var pageSize = this.options.sizeModes.filter(function (item) {
      return item.className.indexOf(sizeMode) > -1;
    });
    pageSize = pageSize[0]
    this.mapContainer.style.width = pageSize.width + 'px';
    this.mapContainer.style.height = pageSize.height + 'px';
    if (this.mapContainer.style.width > this.mapContainer.style.height) {
      this.orientation = 'portrait';
    } else {
      this.orientation = 'landscape';
    }
    if (areBoundsValid(this.options.bounds)) {
      this._map.invalidateSize();
      this._map.fitBounds(this.options.bounds);
    } else {
      this._map.setView(this.originalState.center);
      this._map.setZoom(this.originalState.zoom);
      this._map.invalidateSize();
    }
    if (this.options.tileLayer) {
      this._pausePrint(sizeMode)
    } else {
      this._printOperation(sizeMode)
    }
  },

  _pausePrint: function (sizeMode) {
    var plugin = this
    plugin._map.fire("easyPrint-status-update", { status: "Applying textures" });
    var loadingTest = setInterval(function () { 
      if(!plugin.options.tileLayer.isLoading()) {
        clearInterval(loadingTest);
        plugin._printOperation(sizeMode)
      }
    }, plugin.options.tileWait);
  },

  _printOperation: function (sizemode) {
    var plugin = this;
    var widthForExport = this.mapContainer.style.width;
    plugin._map.fire("easyPrint-status-update", { status: "Generating image" });
    domtoimage.toPng(plugin.mapContainer, {
      width: parseInt(widthForExport),
      height: parseInt(plugin.mapContainer.style.height),
      dpi: plugin.options.dpi
    })
    .then(function (dataUrl) {
      document.getElementById('new-map').src = dataUrl;
      plugin._toggleControls(true);

      plugin._map.fire("easyPrint-status-update", { status: "Cleaning up" });

      if (plugin.outerContainer) {
        if (plugin.originalState.widthWasPercentage) {
          plugin.mapContainer.style.width = plugin.originalState.percentageWidth
        } else {
          plugin.mapContainer.style.width = plugin.originalState.mapWidth;
        }
        plugin.mapContainer.style.height = plugin.originalState.mapHeight;
        plugin._removeOuterContainer(plugin.mapContainer, plugin.outerContainer, plugin.blankDiv)
        plugin._map.invalidateSize();
        plugin._map.setView(plugin.originalState.center);
        plugin._map.setZoom(plugin.originalState.zoom);
      }
      const mode = plugin.options.dpi > 200 ? "final" : "preview";
      plugin._map.fire("easyPrint-finished", { mode: mode });
    })
    .catch(function (error) {
      plugin._map.fire("easyPrint-failed", { error: error });
    });
  },

  _createOuterContainer: function (mapDiv) {
    var outerContainer = document.createElement('div'); 
    mapDiv.parentNode.insertBefore(outerContainer, mapDiv); 
    mapDiv.parentNode.removeChild(mapDiv);
    outerContainer.appendChild(mapDiv);
    outerContainer.style.width = mapDiv.style.width;
    outerContainer.style.height = mapDiv.style.height;
    outerContainer.style.display = 'inline-block'
    outerContainer.style.overflow = 'hidden';
    return outerContainer;
  },

  _removeOuterContainer: function (mapDiv, outerContainer, blankDiv) {
    if (outerContainer.parentNode) {
      outerContainer.parentNode.insertBefore(mapDiv, outerContainer);
      outerContainer.parentNode.removeChild(blankDiv);
      outerContainer.parentNode.removeChild(outerContainer);      
    }
  },

  _getImageContainer: function () {
    var container = document.createElement('img');
    container.style.display = 'none';
    container.id = 'new-map';
    return container;
  },

  _toggleControls: function (show) {
    var controlContainer = document.getElementsByClassName("leaflet-control-container")[0];
    if (show) return controlContainer.style.display = 'block';
    controlContainer.style.display = 'none';
  },

  _3by4: {
    height: 2400,
    width: 3200
  },

  _1by1: {
    height: 3000,
    width: 3000
  },

  _16by9: {
    height: 2250,
    width: 4000
  }

});

L.easyPrint = function(options) {
  return new L.Control.EasyPrint(options);
};
