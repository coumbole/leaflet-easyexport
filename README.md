## leaflet-easyExport
A simple [leaflet](http://www.leafletjs.com) plugin allows you to export
the map as an image. Forked from leaflet-easyPrint. This is heavily
modified, so it's not compatible with leaflet-easyPrint.

## Features
* Supports resizing to predefined sizes.
* Supports saving to png
* Compatible with at least Leaflet v1+
* Tested on Chrome, Firefox
  * Incompatible with IE & Edge due to `dom-to-image` dependency

Check out the [demo](http://rowanwins.github.com/leaflet-easyPrint/).

### Options
You can pass a number of options to the plugin to control various settings.

| Option        | Type         | Default      | Description   |
| ------------- |--------------|--------------|---------------|
| title | string | 'Print map' | Sets the text which appears as the tooltip of the print/export button |
| sizeModes | array | `Current` | Options available include `3x4`, `4x3` or a [custom size object](#custom-print-sizes) |
| tileLayer | [Leaflet tile layer](http://leafletjs.com/reference-1.1.0.html#tilelayer) | `null` | A tile layer that you can wait for to draw (helpful when resizing) |
| tileWait | Integer | 500 | How long to wait for the tiles to draw (helpful when resizing) |
| filename | string | 'map' | Name of the file if export only option set to true |
| dpi | Integer | '220' | DPI for the exported file |
| bounds | [Leaflet LatLngBounds](https://leafletjs.com/reference-1.4.0.html#latlngbounds) | [[],[]] | The outer bounds for the exported image |


### Example
````
L.easyPrint({
	title: 'My awesome print button',
  dpi,
  bounds: [[40.7142, -74.0059], [40.7300, -73.5000]]
}).addTo(map);
````

### Methods / Using programmatically
| Method        | Options      | Description   |
| --------------|--------------|--------------|
| printMap(size, filename) | Print size name, either '3x4', '4x3', or the `className` of a custom size. And a filename | Manually trigger a print operation |
````
var printPlugin = L.easyPrint({
	hidden: true,
	sizeModes: ['3x4']
}).addTo(map); 
printPlugin.printMap('3x4', 'MyFileName');
````


### Custom Print Sizes
You can create additional print sizes by passing in some options.
````
var a3Size = {
	width: 2339,
	height: 3308,
	className: 'a3CssClass',
	tooltip: 'A custom A3 size'
}
````

### Acknowledgements
Huge hats off go to the original author [rowanwins](https://github.com/rowanwins) and all the [contributors](https://github.com/Leaflet/Leaflet/graphs/contributors) to the leaflet.js project.

Also uses [dom-to-image-h4](https://github.com/h4buli/dom-to-image) and [FileSaver](https://github.com/eligrey/FileSaver.js) under the hood. Original dom-to-image do not support custom DPI settings as of now (Nov. 2018), so this project uses the downstream fork. [H4buli](https://github.com/h4buli/dom-to-image) has submitted a PR to the original dom-to-image project, so if that gets accepted this project likely moves back to the original version.
