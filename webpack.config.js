const path = require('path');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')

const lintJSOptions = {
  emitWarning: true,

  // Fail only on errors
  failOnWarning: true,
  failOnError: true,

  // Toggle autofix
  fix: true,
  cache: true,

  formatter: require('eslint-friendly-formatter')
}

const lintJS = {
  module: {
    rules: [
      {
        test: /\.js$/,
				exclude: /node_modules/,
        enforce: 'pre',
        loader: 'eslint-loader',
				options: lintJSOptions
      }
    ]
  }
}

const loadJS = {
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			}
		]
	}
}

const commonConfig = merge([
  {
    entry: './src/index.js',
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new FriendlyErrorsPlugin()
    ],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    }
  },
  lintJS,
	loadJS
])

const developmentConfig = {
  mode: 'development',
  devtool: 'inline-source-map'
}

const productionConfig = {
  mode: 'production',
  devtool: 'source-map',
	performance: {
		hints: 'warning', // 'error' or false are valid too
		maxEntrypointSize: 100000, // in bytes
		maxAssetSize: 450000 // in bytes
	}
}

module.exports = env => {
  process.env.NODE_ENV = env;
  const config = env === 'production' ? productionConfig : developmentConfig;
  return merge(
    commonConfig,
    config
  );
};
